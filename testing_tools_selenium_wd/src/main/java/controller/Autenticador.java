/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*; 


/**
 *
 * @author fabriciogmc
 */
@WebServlet(urlPatterns={"/Autenticar"})
public class Autenticador extends HttpServlet {
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp){
    	
    	 String st = ""; 
    	 
        try {
        	File file = new File("C:\\Users\\RAFAELVIEIRASINOSAKI\\Desktop\\testing_tools_selenium_wd\\src\\data\\test.csv"); 
           	
            BufferedReader br = new BufferedReader(new FileReader(file)); 

            st = br.readLine();
           
        } catch (IOException e) {
            System.out.println("Can't read a"); // Or something more intellegent
        }
        
        try{
            req.setCharacterEncoding("UTF-8"); 
        }catch(Exception e){} 
        String nomeUsuario = req.getParameter("nomeUsuario");
        String senha = req.getParameter("senha");
        ServletContext sc = req.getServletContext();
        if ( nomeUsuario.equals("fabricio") && senha.equals("123")){
        	String[] arrOfStr = st.split(",");
            req.setAttribute("nomeUsuario", arrOfStr[0]);
            req.setAttribute("senha", arrOfStr[1]);
            req.setAttribute("Idade", arrOfStr[2]);
            req.setAttribute("Altura", arrOfStr[3]);
            
            try {
                sc.getRequestDispatcher("/jsp/paginaInicial.jsp").forward(req, resp); 
            }catch(Exception e){
                //Tratamento de erro de IO ou de Servlet..
            }                       
        }
        else{
            try {
                req.setAttribute("falhaAutenticacao", true);
                sc.getRequestDispatcher("/jsp/login.jsp").forward(req, resp);
            }catch(Exception e){
                //Tratamento de erro de IO ou de Servlet..
            }  
        }    
    }
}