/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.*;

/**
 *
 * @author fabricio
 */
public class LoginTestDriver {
    
    @Test
    public void testLogin(){
        System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("http://localhost:8083/testing_tools_selenium_wd/");

        WebElement nomeUsuario = driver.findElement(By.id("nomeUsuario"));
        nomeUsuario.sendKeys("fabricio");

        WebElement senhaUsuario = driver.findElement(By.id("senha"));
        //senha incorreta
        senhaUsuario.sendKeys("1234");
        //senha correta
        //senhaUsuario.sendKeys("123");

        driver.findElement(By.id("autentica")).click();
        
        
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.alertIsPresent());

        Alert alert = driver.switchTo().alert();
       
        String text = alert.getText();
        
        System.out.println(text);
        
        alert.dismiss();
        
        WebElement valorTxt = driver.findElement(By.id("valorFinal"));
        valorTxt.sendKeys(text);


        //driver.quit();
    }
}
